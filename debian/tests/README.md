## Notes on how this package can be tested.
────────────────────────────────────────

This package can be tested by running the provided test:

#### Note! This is a real-world example, and requires around 12 hours of run time.

`sh run-example`

Test documentation can be referenced [here](https://github.com/xavierdidelot/clonalframeml/wiki)